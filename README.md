# Deploy WebPageTest Server and Agent on AWS with Docker
## _Install Docker on AWS to deploy WPT_

Clone this Terraform to your host and make the changes on terraform.tfvars:

- my_ip  - Set your network IP
- key_pub - Set your pub key for ssh access.
- name = Set name for application

In main.tf:
- Change the name of resouces
- Change or not the ingress IP/Netowork for production

### Functions

- Terraform deploy the Instance e copy install.sh for config/install script
- Deploy the containers from Docker Hub and Dockerfile from Git oficial repo

## Running Terraform
Prerequisites:
- Terraform CLI installed.
- AWS CLI installed [https://docs.aws.amazon.com/cli/latest/userguide/install-cliv2.html]
- Secret access key and Access key ID for aws configure

Terraform script will copy the install.sh file, how will update and install docker from oficial site. Then it will deploy the Server container from Docker Hub repo and build the Agent from Git for deploy.
At this moment only Firefox option is working.

```sh
aws configure
terraform plan
terraform apply
```
All scripts are commented, adjust by your need.
Second step will be de K8S senario.