#Set region to deploy
region = "sa-east-1"

#Name for the application
name = "np-server01"

#Env for depploy
env = "dev"

#ID of AMI image. In this case Ubuntu
ami = "ami-054a31f1b3bf90920"

#Instance type to use. T2 micro is the free one
instance_type = "t2.micro"

#IP for firewall configuration
my_ip = "YOUR_IP"

#RSA PUB for deploy with your ssh key
key_pub = "YOUR_PUB_KEY"