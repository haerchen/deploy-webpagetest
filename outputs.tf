#Get the Public IP to access
output "instance_eip_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_eip.npserver01.public_ip
}

#Get the Public DNS to access
output "instance_eip_public_dns" {
  description = "Public IP address of the EC2 instance"
  value       = aws_eip.npserver01.public_dns
}