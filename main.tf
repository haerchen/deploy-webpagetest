variable    "region"   {}
variable    "name"   {}
variable    "env"    {}
variable    "ami"   {}
variable    "instance_type"   {}
variable    "my_ip"   {}
variable    "key_pub"   {}

provider "aws" {
  profile = "default"
  region  = var.region
}

#Import your pub key
resource "aws_key_pair" "npserver01" {
  key_name   = "npserver01"
  public_key = var.key_pub
}

#Configure security access
resource "aws_security_group" "npserver01" {
  name        = "main-security-group"
  description = "Allow HTTP, HTTPS and SSH traffic"

  ingress {
    description = "SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    # For secrurity propose use only your IP/Network
    cidr_blocks = [var.my_ip]
  }

  ingress {
    description = "HTTPS"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    #Change to 0.0.0.0/0 to turn public
    cidr_blocks = [var.my_ip]
  }

  ingress {
    description = "HTTP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    #Change to 0.0.0.0/0 to turn public
    cidr_blocks = [var.my_ip]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "terraform"
  }
}

#Create the instance
resource "aws_instance" "npserver01" {
  key_name      = aws_key_pair.npserver01.key_name
  ami           = var.ami
  instance_type = "t2.micro"

  tags = {
    Name = "npserver01"
  }

  vpc_security_group_ids = [
    aws_security_group.npserver01.id
  ]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = var.key_pub
    host        = self.public_ip
  }

  provisioner "file" {
      source      = "install.sh"
      destination = "/tmp/install.sh"
      connection {
        type = "ssh"
        user = "ubuntu"
        private_key = file("~/.ssh/id_rsa")
        host = self.public_ip
      }
      on_failure = continue
  }

  provisioner "remote-exec" {
      inline = [
          "chmod +x /tmp/install.sh",
          "/tmp/install.sh"
      ]

      connection {
          type = "ssh"
          user = "ubuntu"
          #Connect using your local PV key
          private_key = file("~/.ssh/id_rsa")
          host = self.public_ip
      }
      on_failure = continue
  
  }
  #Config Disk
  ebs_block_device {
    device_name = "/dev/sda1"
    volume_type = "gp2"
    volume_size = 20
  }

}

resource "aws_eip" "npserver01" {
  vpc      = true
  instance = aws_instance.npserver01.id
}

