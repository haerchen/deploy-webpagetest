#!/bin/bash
#Update server
sudo apt update && sudo apt upgrade -y
#Set a valid Hostname
sudo hostnamectl set-hostname np-server01
#Config needed for WPT Agent
sudo modprobe ifb numifbs=1
#Docker instalation
sudo curl -fSsl https://get.docker.com | sudo bash
#Run WPT Server from docker repo
sudo docker container run -d -p 80:80 webpagetest/server
#Another option is build a image by your own. I use the default git repo
git clone https://github.com/WPO-Foundation/wptagent
cd wptagent/
#Building the image from git repo
sudo docker image build -t wptagent:1.0 .
#Running your own image
sudo docker container run -d -p 81:80 --network="host" -e "SERVER_URL=http://localhost/work/" -e "LOCATION=Test" wptagent:1.0